/*

November 2017 © Andrea Martino (cicci8ino)
cicci8ino@gmail.com
CC BY-NC-SA 3.0

CHALLENGE RIM STANDALONE

This poorly written sketch makes your CHALLENGE rim standalone.
It is heavily based on tx_rw_wheel_reader.ino by Taras Ivaniukovich which license is written below.

---
This sketch provided "AS IS" under the BSD New license.
http://opensource.org/licenses/BSD-3-Clause
April 2015 © blog@rr-m.org

download tx_rw_wheel_reader.ino above - it has more comments
---

This sketches uses Arduino HID-Project by NicoHood.
Here you can find the GitHub repository: https://github.com/NicoHood/HID

*/

#include <SPI.h>
#include <HID-Project.h>
#include <Arduino.h>
#include <types.h>

bool atLeastOneChange=false; //Used to decide if a new Gamepad status must be sent
byte prevData[8]; //Stores previous data

const int slaveSelectPin = 7;

Button byteZero[BUTTONS_BYTEZERO] { //Buttons in the first byte
  Button(PIT, PIT_MASK),
  Button(SCROLL, SCROLL_MASK),
  Button(WATER, WATER_MASK),
  Button(RPADDLE, RPADDLE_MASK),
  Button(LPADDLE, LPADDLE_MASK)
};

Button byteOne[BUTTONS_BYTEONE] { //Buttons in the first byte
  Button(FLASH, FLASH_MASK),
  Button(RADIO, RADIO_MASK),
  Button(WIPER, WIPER_MASK),
  Button(TC_ONE, TC_ONE_MASK),
  Button(TC_TWO, TC_TWO_MASK),
  Button(TC_OFF, TC_OFF_MASK),
};

Dpad dpad = Dpad(DPAD_U_MASK, DPAD_D_MASK, DPAD_L_MASK, DPAD_R_MASK);

void setup() {
  SPCR |= _BV(CPHA);
  SPCR |= _BV(CPOL);
  SPI.beginTransaction(SPISettings(40000, MSBFIRST, SPI_MODE0));
  SPI.begin();
  pinMode(slaveSelectPin, OUTPUT);
  Gamepad.begin();
}

void loop() {
  digitalWrite(slaveSelectPin, LOW);
  for(int byteIndex = 0; byteIndex<=7; byteIndex++) {
    delayMicroseconds(100);
    handleBinaryData(SPI.transfer(0x00), byteIndex);
  }
  if (atLeastOneChange) {
    Gamepad.write();
    atLeastOneChange=false;
  }
  digitalWrite(slaveSelectPin, HIGH);
  delayMicroseconds(1500);
}

void handleBinaryData(byte data, int byteIndex) {
  byte oldData = prevData[byteIndex];
  if (data!=oldData) {
    atLeastOneChange=true;
    byte changed = data^oldData;
    byte byteSwap = (oldData^data)&oldData;

    if (byteIndex==0) { //handle byte 0
      atLeastOneChange=true;
      for (int i = 0; i<BUTTONS_BYTEZERO; i++) {
        if (byteZero[i].hasChanged(changed)) { //check if changes can depends from buttons
          setButtonState(byteZero[i].getButton(), changed&byteSwap);
        }
      }
    }

    if (byteIndex==1) {
      atLeastOneChange=true;
      for (int i = 0; i<BUTTONS_BYTEONE; i++) {
        if (byteOne[i].hasChanged(changed)) {
          setButtonState(byteOne[i].getButton(), changed&byteSwap);
        }
      }
    }

    if (byteIndex==2) {
      atLeastOneChange=true;
      Gamepad.dPad1(dpad.getPosition(data)); //get dpads positions
    }
  }
  prevData[byteIndex]=data;
}

void setButtonState(int button, bool state) {
  if (state)
    Gamepad.press(button);
  else
    Gamepad.release(button);
}
