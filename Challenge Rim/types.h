#include <Arduino.h>
#include <data.h>

class Button {
public:
  bool hasChanged(byte change);
  Button(short id, byte mask);
  short getButton();
private:
  short button;
  byte mask;
};

short Button::getButton() {
  return button;
}

bool Button::hasChanged(byte change) {

  return change&mask;

}

Button::Button(short id, byte mask) {
  this->button = id;
  this->mask = mask;
}

class Dpad {
private:
  byte upmask;
  byte downmask;
  byte leftmask;
  byte rightmask;
public:
  Dpad(byte upmask, byte downmask, byte leftmask, byte rightmask);
  short getPosition(byte data);
};

Dpad::Dpad(byte upmask, byte downmask, byte leftmask, byte rightmask) {
  this->downmask=downmask;
  this->upmask=upmask;
  this->leftmask=leftmask;
  this->rightmask=rightmask;
}

short Dpad::getPosition(byte data) {/*
  if ((~data&(downmask|leftmask))==(downmask|leftmask))
    return DOWN_LEFT;
  if ((~data&(downmask|rightmask))==(downmask|rightmask))
    return DOWN_RIGHT;
  if ((~data&(upmask|leftmask))==(upmask|leftmask))
    return UP_LEFT;
  if ((~data&(upmask|rightmask))==(upmask|rightmask))
    return UP_RIGHT;*/
  if (~data&upmask)
    return UP;
  if (~data&downmask)
    return DOWN;
  if (~data&leftmask)
    return LEFT;
  if (~data&rightmask)
    return RIGHT;
  return CENTER;
}
