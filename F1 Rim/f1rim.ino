/*

November 2017 © Andrea Martino (cicci8ino)
cicci8ino@gmail.com
CC BY-NC-SA 3.0

F1 RIM STANDALONE

This poorly written sketch makes your F1 rim standalone.
It is heavily based on tx_rw_wheel_reader.ino by Taras Ivaniukovich which license is written below.

---
This sketch provided "AS IS" under the BSD New license.
http://opensource.org/licenses/BSD-3-Clause
April 2015 © blog@rr-m.org

download tx_rw_wheel_reader.ino above - it has more comments
---

This sketches uses Arduino HID-Project by NicoHood.
Here you can find the GitHub repository: https://github.com/NicoHood/HID

*/

#include "types.h"
#include <SPI.h>
#include <HID-Project.h>

Button byteZero[BUTTONS_BYTEZERO] { //Buttons in the first byte
  Button(DRS, DRS_MASK),
  Button(N, N_MASK),
  Button(LPADDLE, LPADDLE_MASK),
  Button(RPADDLE, RPADDLE_MASK),
  Button(PIT, PIT_MASK)
};

Button byteOne[BUTTONS_BYTEONE] { //Buttons in the first byte
  Button(PL, PL_MASK),
  Button(PUMP, PUMP_MASK),
  Button(K, K_MASK),
  Button(MINONE, MINONE_MASK),
  Button(PLUSTEN, PLUSTEN_MASK),
  Button(BO, BO_MASK),
  Button(START, START_MASK),
  Button(WET, WET_MASK)
};

Encoder chrg = Encoder(CHRG_ENCODER_CW, CHRG_ENCODER_CCW, CHRG_ENCODER_CW_MASK, CHRG_ENCODER_CCW_MASK, 3, 200);
Encoder difIn= Encoder(DIF_ENCODER_CW, DIF_ENCODER_CCW, DIF_ENCODER_CW_MASK, DIF_ENCODER_CCW_MASK, 3, 200);
Dpad ldpad = Dpad(LDPAD_U_MASK, LDPAD_D_MASK, LDPAD_L_MASK, LDPAD_R_MASK);
Dpad rdpad = Dpad(RDPAD_U_MASK, RDPAD_D_MASK, RDPAD_L_MASK, RDPAD_R_MASK);;


bool atLeastOneChange=false; //Used to decide if a new Gamepad status must be sent
byte prevData[8]; //Stores old data

const int slaveSelectPin = 7;

void setup() {
  SPCR |= _BV(CPHA);
  SPCR |= _BV(CPOL);
  SPI.beginTransaction(SPISettings(40000, MSBFIRST, SPI_MODE0));
  SPI.begin();
  pinMode(slaveSelectPin, OUTPUT);
  Gamepad.begin();
}


void loop() {

  //update encoders to decide if wether to release them or not
  //cw = clockwise
  //ccw = counterclockwise

  chrg.update();
  if (short enc = chrg.getButtonToRelease()) {
    Gamepad.release(enc); //release encoder
    chrg.reset(); //reset encoder
    Gamepad.write();
  }
  difIn.update();
  if (short enc = difIn.getButtonToRelease()) {
    Gamepad.release(enc);
    difIn.reset();
    Gamepad.write();
  }

  digitalWrite(slaveSelectPin, LOW);
  for(int byteIndex = 0; byteIndex<=7; byteIndex++) {
    delayMicroseconds(100);
    handleBinaryData(SPI.transfer(0x00), byteIndex); //handle single bytes
  }

  if (atLeastOneChange) { //if at least one change is detected then report new gamepad status
    Gamepad.write();
    atLeastOneChange=false;
  }


  digitalWrite(slaveSelectPin, HIGH);
  delayMicroseconds(1500);
}


void printBinary(byte data) {
 for(int i=7; i>0; i--) {
   if (data >> i == 0) {
     Serial.print("0");
   } else {
     break;
   }
 }
 Serial.print(data,BIN);
 Serial.print(" ");
}

void handleBinaryData(byte data, int byteIndex) {
  byte oldData = prevData[byteIndex];
  if (data!=oldData) { //if read byte is different from the old byte
    byte changed = data^oldData; //something it's changed
    byte byteSwap = (oldData^data)&oldData; //did bit change 0->1 or 1->0?
    if (byteIndex==0) { //handle byte 0
      atLeastOneChange=true;
      for (int i = 0; i<BUTTONS_BYTEZERO; i++) {
        if (byteZero[i].hasChanged(changed)) { //check if changes can depends from buttons
          setButtonState(byteZero[i].getButton(), changed&byteSwap);
        }
      }
    }

    if (byteIndex==1) {
      atLeastOneChange=true;
      for (int i = 0; i<BUTTONS_BYTEONE; i++) {
        if (byteOne[i].hasChanged(changed)) {
          setButtonState(byteOne[i].getButton(), changed&byteSwap);
        }
      }


    }

    if (byteIndex==2) {
      atLeastOneChange=true;
      Gamepad.dPad1(ldpad.getPosition(data)); //get dpads positions
      Gamepad.dPad2(rdpad.getPosition(data));
    }

    if (byteIndex==3) {
      chrg.update(changed);
      difIn.update(changed);
      if (int enc = chrg.getButtonToPress(changed)) {
        Gamepad.press(enc);
        atLeastOneChange=true;
      }

      if (int enc = difIn.getButtonToPress(changed)) {
        Gamepad.press(enc);
        atLeastOneChange=true;
      }

    }
  }
  prevData[byteIndex]=data;
}

void setButtonState(int button, bool state) {
  if (state)
    Gamepad.press(button);
  else
    Gamepad.release(button);
}
