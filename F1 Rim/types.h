#include <Arduino.h>
#include <data.h>

class Encoder {

public:

    Encoder(short buttonCw, short buttonCcw, byte maskCw, byte maskCcw, short maxSteps, unsigned long recordingTime);
    void incCwCounter();
    void incCcwCounter();
    bool isCwReady();
    bool isCcwReady();
    bool isCwReadyToRelease();
    bool isCcwReadyToRelease();
    void updateCcw();
    void updateCw();
    void update(byte mask);
    void update();
    short hasChanged(byte mask);
    short getButtonToPress(byte mask);
    short getButtonToRelease();
    void reset();
  private:
    short stepsCw;
    short stepsCcw;
    bool readyCw;
    bool readyCcw;
    bool readyToReleaseCw;
    bool readyToReleaseCcw;
    unsigned long tsCw;
    unsigned long tsCcw;
    unsigned long recordingTime;
    short buttonCw;
    short buttonCcw;
    short maxSteps;
    byte maskCw;
    byte maskCcw;

};

Encoder::Encoder(short buttonCw, short buttonCcw, byte maskCw, byte maskCcw, short maxSteps, unsigned long recordingTime) {
  this->buttonCw=buttonCw;
  this->buttonCcw=buttonCcw;
  this->maskCw=maskCw;
  this->maskCcw=maskCcw;
  this->maxSteps=maxSteps;
  this->recordingTime=recordingTime;
}

bool Encoder::isCwReadyToRelease() {
  return readyToReleaseCw;
}

bool Encoder::isCcwReadyToRelease() {
  return readyToReleaseCcw;
}

bool Encoder::isCcwReady() {
  return readyCcw;
}

bool Encoder::isCwReady() {
  return readyCw;
}

void Encoder::update() {
  if (isCwReady()) {
    if (millis()-tsCw>ENCODER_PRESSED_TIME) {
      readyToReleaseCw=true;
    }
  }
  if (isCcwReady()) {
    if (millis()-tsCcw>ENCODER_PRESSED_TIME) {
      readyToReleaseCcw=true;
    }
  }
}

void Encoder::reset() {
  readyCw=false;
  readyCcw=false;
  readyToReleaseCcw = false;
  readyToReleaseCw = false;
}

short Encoder::getButtonToRelease() {

  if (isCwReadyToRelease()) {
    return buttonCw;
  }
  if (isCcwReadyToRelease()) {
    return buttonCcw;
  }
  return 0;
}


short Encoder::getButtonToPress(byte mask) {

  if (isCwReadyToRelease()|isCcwReadyToRelease())
    return 0;
  if (isCwReady())
    return buttonCw;
  if (isCcwReady())
    return buttonCcw;
  return 0;

}



void Encoder::update(byte mask) {
  if (isCcwReady() | isCwReady() | isCcwReadyToRelease() | isCwReadyToRelease())
    return;
  if (mask&maskCw) {
    updateCw();
    return;
  }
  if (mask&maskCcw) {
    updateCcw();
    return;
  }
}

short Encoder::hasChanged(byte mask) {
  if (mask&maskCw)
    return CW_CHANGED;
  if (mask&maskCcw)
    return CCW_CHANGED;
  return 0;
}

void Encoder::updateCw() {
  unsigned long ts = millis();
  if (ts-tsCw>recordingTime) {
    stepsCw = 1;
    tsCw = ts;
  }
  else {
    incCwCounter();
  }
}

void Encoder::updateCcw() {
  unsigned long ts = millis();
  if (ts-tsCcw>recordingTime) {
    stepsCcw = 1;
    tsCcw = ts;
  }
  else {
    incCcwCounter();
  }

}

void Encoder::incCwCounter() {
  stepsCw++;
  if (stepsCw>=maxSteps*2) {
    readyCw=true;
    readyCcw=false;
    stepsCw=0;
    stepsCcw=0;
  }

}

void Encoder::incCcwCounter() {
  stepsCcw++;
  if (stepsCcw>=maxSteps*2) {
    readyCw=false;
    readyCcw=true;
    stepsCcw=0;
    stepsCw=0;
  }
}

class Button {
public:
  bool hasChanged(byte change);
  Button(short id, byte mask);
  short getButton();
private:
  short button;
  byte mask;
};

short Button::getButton() {
  return button;
}

bool Button::hasChanged(byte change) {

  return change&mask;

}

Button::Button(short id, byte mask) {
  this->button = id;
  this->mask = mask;
}

class Dpad {
private:
  byte upmask;
  byte downmask;
  byte leftmask;
  byte rightmask;
public:
  Dpad(byte upmask, byte downmask, byte leftmask, byte rightmask);
  short getPosition(byte data);
};

Dpad::Dpad(byte upmask, byte downmask, byte leftmask, byte rightmask) {
  this->downmask=downmask;
  this->upmask=upmask;
  this->leftmask=leftmask;
  this->rightmask=rightmask;
}

short Dpad::getPosition(byte data) {
  if ((~data&(downmask|leftmask))==(downmask|leftmask))
    return DOWN_LEFT;
  if ((~data&(downmask|rightmask))==(downmask|rightmask))
    return DOWN_RIGHT;
  if ((~data&(upmask|leftmask))==(upmask|leftmask))
    return UP_LEFT;
  if ((~data&(upmask|rightmask))==(upmask|rightmask))
    return UP_RIGHT;
  if (~data&upmask)
    return UP;
  if (~data&downmask)
    return DOWN;
  if (~data&leftmask)
    return LEFT;
  if (~data&rightmask)
    return RIGHT;
  return CENTER;
}
