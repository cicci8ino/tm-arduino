#include <Arduino.h>

enum {
  NOT_USED,
  DRS,
  N,
  LPADDLE,
  RPADDLE,
  PIT,
  PL,
  K,
  PUMP,
  MINONE,
  PLUSTEN,
  BO,
  START,
  WET,
  LDPAD_L,
  LDPAD_U,
  LDPAD_D,
  LDPAD_R,
  LDPAD_C,
  RDPAD_L,
  RDPAD_U,
  RDPAD_D,
  RDPAD_R,
  RDPAD_C,
  CHRG_ENCODER_CW,
  CHRG_ENCODER_CCW,
  DIF_ENCODER_CCW,
  DIF_ENCODER_CW
};

enum {
  CENTER,
  UP,
  UP_RIGHT,
  RIGHT,
  DOWN_RIGHT,
  DOWN,
  DOWN_LEFT,
  LEFT,
  UP_LEFT
};

const short ENCODER_STEPS = 3;
const unsigned long ENCODER_RECORDING_TIME = 200;
const unsigned long ENCODER_PRESSED_TIME = 200;
const short CW_CHANGED = 1;
const short CCW_CHANGED = 2;

//BYTE 3

const byte DIF_ENCODER_CW_MASK = B00010000;
const byte DIF_ENCODER_CCW_MASK = B00100000;
const byte CHRG_ENCODER_CW_MASK = B00001000;
const byte CHRG_ENCODER_CCW_MASK = B01000000;

//BYTE 2
const byte RDPAD_U_MASK = B10000000;
const byte RDPAD_D_MASK = B00000100;
const byte RDPAD_R_MASK = B00000010;
const byte RDPAD_L_MASK = B00000001;

const byte LDPAD_U_MASK = B00001000;
const byte LDPAD_D_MASK = B01000000;
const byte LDPAD_R_MASK = B00100000;
const byte LDPAD_L_MASK = B00010000;

const short BUTTONS_BYTEZERO = 5;
const short BUTTONS_BYTEONE = 8;

//BYTE 1
const byte PL_MASK = B00001000;
const byte K_MASK = B00000100;
const byte PUMP_MASK = B00000010;
const byte MINONE_MASK = B00000001;
const byte PLUSTEN_MASK = B01000000;
const byte BO_MASK = B00100000;
const byte START_MASK = B10000000;
const byte WET_MASK = B00010000;

//BYTE 0
const byte DRS_MASK = B10000000;
const byte N_MASK = B00000010;
const byte LPADDLE_MASK = B00001000;
const byte RPADDLE_MASK = B00000100;
const byte PIT_MASK = B00000001;
